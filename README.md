# Auth

Auth provider central class for FAE
This central class can be extended by an authentication provider, such as Local Authentication or OIDC.