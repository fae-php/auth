<?php

namespace FAE\auth;

class auth
{

  /** @var \FAE\auth\authProvider Object that implements authInterface and extends authProvider */
  private $instance;

  /**
   * At construct time the instance of the authentication provider is established and can be accessed through the get() method
   *
   * @param boolean $force    Whether to force authentication for the user or not. This will forcibly redirect the user to the authentication mechanism if they are not logged in already
   */
  public function __construct(bool $force = false)
  {
    global $config;

    $authClass = property_exists($config, 'authClass') ? $config->authClass : '\FAE\auth_local\auth';
    try {
      $instance = new $authClass($force);
    } catch (\Error $e){
      var_dump("Could not find the auth class '{$authClass}'");
      throw new \Error($e);
    }
    if(!is_subclass_of($instance, __NAMESPACE__.'\authProvider')){
      trigger_error("Auth class '{$authClass}' does not extend authProvider", E_USER_WARNING);
    }
    $this->instance = $instance;
  }

  /**
   * Auth provider instantiation class
   *
   * @return \FAE\auth\authProvider
   */
  public function get(){
    return $this->instance;
  }
}
