<?php

namespace FAE\auth;

use \RuntimeException;

class authException extends RuntimeException
{
  public function __construct(string $message, ?int $code = 0, ?RuntimeException $previous = null)
  {
    parent::__construct($message, $code, $previous);
  }
}
