<?php

namespace FAE\auth;

use FAE\user\user;
use FAE\permissions\perm_assign;
use FAE\permissions\perm_group;

abstract class authProvider
{

  // @var int $user_id
  protected $user_id;
  // @var string $username
  protected $username;
  // @var int $perm_group_id
  protected $perm_group_id;

  static $default_pgid;

  /**
   * Class variables for user_id and perm_group_id will be initialised based on the current authenticated user
   *
   * @param boolean $force   If true this will redirect the client to the OIDC provider for authentication
   * @return boolean
   */
  public function __construct(bool $force = false)
  {
    if (is_numeric($this->user_id) && is_numeric($this->perm_group_id) && is_string($this->username)) {
      return;
    }
    if (!is_numeric($this->user_id)) {
      if (!$this->getSession()) {
        if ($force) {
          $this->redirectLogin();
        }
        return;
      }
    }
    if ($this->user_id && (!is_numeric($this->perm_group_id) || !is_string($this->username))) {
      $uh = new user();
      $uh->setDirect(true);
      $userdata = $uh->getById($this->user_id);
      if (!$userdata || !array_key_exists('perm_group_id', $userdata)) {
        if ($force) {
          $this->redirectLogin();
        }
        return;
      }
      $this->perm_group_id = $userdata['perm_group_id'];
      $this->username = $userdata['username'];
    }
  }

  /**
   * Return a boolean whether the user is logged in or not
   *
   * @return boolean
   */
  public function isLoggedIn(): bool
  {
    return is_int($this->user_id);
  }

  /**
   * Get User ID
   *
   * @return integer|null
   */
  public function getUserID(): ? int
  {
    return $this->user_id;
  }

  /**
   * Get username
   *
   * @return integer|null
   */
  public function getUsername(): ? string
  {
    return $this->username;
  }

  /**
   * Verify a permission level for the current user against a specific permission reference
   *
   * @param mixed $perm_id    Permission ID reference, either integer or string
   * @param integer $level    Level to be tested against
   * @param boolean $force    Force authentication redirect
   * @return boolean
   */
  public function verify($perm_id, int $level, bool $force = false): bool
  {
    $pah = new perm_assign();
    $filter = ['perm_group_id' => $this->getPermGroup()];
    if (is_int($perm_id)) {
      $filter['perm_id'] = $perm_id;
    } else {
      $filter['key'] = $perm_id;
    }
    if ($this->getPermGroup() && $pah->get($filter)->fetch()['level'] >= $level) {
      return true;
    }
    return false;
  }

  /**
   * Get the permission group of the currently authenticated user or the default configured permission group if none is set
   *
   * @return int $perm_group_id
   */
  public function getPermGroup(): ?int
  {
    if (is_int($this->perm_group_id)) {
      return $this->perm_group_id;
    }
    return self::getDefaultPermGroup();
  }

  /**
   * Get the configured default permission group
   *
   * @return integer|null
   */
  static function getDefaultPermGroup(): ?int
  {
    if (is_int(self::$default_pgid)) {
      return self::$default_pgid;
    }
    $pgh = new perm_group();
    $defaultGroup = $pgh->get(['default' => 1]);
    if (!$defaultGroup->rowCount()) {
      throw new \RuntimeException("Default permission group not defined");
      return null;
    }
    if ($defaultGroup->rowCount() > 1) {
      trigger_error("Multiple default permissions group defined - expect issues", E_USER_WARNING);
    }
    self::$default_pgid = $defaultGroup->fetch()['id'];
    return self::$default_pgid;
  }

  abstract public function getSession();
  abstract public function redirectLogin();
  abstract public function loginURL(?string $destination = null);
}
